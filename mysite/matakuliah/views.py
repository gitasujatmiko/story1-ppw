from django.shortcuts import render, redirect
from .models import Matkul, Tugas
from .forms import MatkulForm
from datetime import datetime, timedelta

def matkul(request):
    matkuls = Matkul.objects.all()
    return render(request, 'matakuliah/matkul.html', {'matkuls': matkuls})

def create_matkul(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('matakuliah:matkuls')
    return render(request, 'matakuliah/matkul-form.html', {'form': form})

def detail_matkul(request, matkul_id):
    detail = Matkul.objects.get(id=matkul_id)
    tugas_matkul = Tugas.objects.filter(mata_kuliah=detail).order_by('deadline')
    #now = datetime.now()
    #prev_day = timedelta(1)
    #prev_days = timedelta(365)
    return render(request, 'matakuliah/detail.html', {'detail': detail, 'tugas_matkul': tugas_matkul})

def delete(request):
    matkuls = Matkul.objects.all()
    return render(request, 'matakuliah/delete.html', {'matkuls': matkuls})

def delete_matkul(request, matkul_id):
    matkul = Matkul.objects.get(id=matkul_id)
    if request.method == 'POST':
        matkul.delete()
        return redirect('matakuliah:delete')
    return render(request, 'matakuliah/delete-confirm.html', {'matkul': matkul})