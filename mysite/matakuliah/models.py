from django.db import models
from .choices import *

class Matkul(models.Model):

    def __str__(self):
        return self.nama_mata_kuliah

    nama_mata_kuliah = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length=100, choices=SEMESTER_CHOICES, default='Ganjil')
    tahun_ajar = models.CharField(max_length=100, choices=TAHUN_CHOICES, default='Gasal')
    ruang = models.CharField(max_length=100)

class Tugas(models.Model):
    mata_kuliah = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    tugas = models.TextField(max_length=200)
    deadline = models.DateTimeField()