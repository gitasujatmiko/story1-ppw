from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama_mata_kuliah', 'dosen', 'jumlah_sks', 'deskripsi', 'semester', 'tahun_ajar', 'ruang']